const fs = require('fs');
const path = require('path');

const createDirectory = (dirName, callback) => {
    const dirPath = path.join(__dirname, dirName);
    fs.mkdir(dirPath, (error) => {
        if (error) {
            console.error(`ERROR: Creating Directory- ${dirName}`, error);
        } else {
            console.log(`SUCCESS: Creating Directory- ${dirName} with directory Path- ${dirPath}`);
        }
        callback(error, dirPath);

    });
}

const createFile = (dirPath, fileName, fileData, callback) => {
    const filePath = path.join(dirPath, fileName);
    fs.writeFile(filePath, fileData, (error) => {
        if (error) {
            console.error(`ERROR: Creating File ${fileName}`, error);
        } else {
            console.log(`SUCCESS: Creating File ${fileName} with file path- ${filePath}`);
        }
        callback(error, filePath);
    });
}

const deleteFile = (filePath, callback) => {
    fs.unlink(filePath, (error) => {
        if (error) {
            console.error(`ERROR: Deleting File ${filePath}`);
        } else {
            console.log(`SUCCESS: Deleting File ${filePath}`);
        }
        callback(error, filePath);
    });
}

const createRandomJSONfiles = (dirPath, callback) => {
    const UPPERLIMITFILES = 10;
    const numberOfFiles = Math.round(Math.random() * UPPERLIMITFILES);
    const iterateArray = new Array(numberOfFiles).fill(1);
    const fileNames = iterateArray.map((element, index) => {
        const RANDOMNUMBERLIMIT = 100;
        const randomNumber = Math.round(Math.random() * RANDOMNUMBERLIMIT);
        const fileName = `${index}_${randomNumber}.json`;
        return fileName;
    });

    const failedFiles = [], createdFiles = [];

    for (const fileName of fileNames) {
        const dateAndTime = new Date().toUTCString();
        let fileContent = {
            fileName: fileName,
            created_at: dateAndTime
        };

        try {
            fileContent = JSON.stringify(fileContent, null, 4);
        } catch (error) {
            console.error(`ERROR: Coverting JSON.stringify`);
        }

        createFile(dirPath, fileName, fileContent, (error, filePath) => {
            if (error) {
                console.error(error);
                failedFiles.push(filePath);
            } else {
                createdFiles.push(filePath);
            }
            if ((failedFiles.length + createdFiles.length) === fileNames.length) {
                const files = {
                    failed_files: failedFiles,
                    created_files: createdFiles
                }
                callback(error, files);
            }

        });
    }
}

const deleteFiles = (filePaths, callback) => {
    const failureDeletion = [], successDeleted = []
    for (const filePath of filePaths) {
        deleteFile(filePath, (error, deletedFilePath) => {
            if (error) {
                failureDeletion.push(deletedFilePath);
            } else {
                successDeleted.push(deletedFilePath);
            }

            if ((failureDeletion.length + successDeleted.length) === filePaths.length) {
                const filesStatus = {
                    failed_deletion: failureDeletion,
                    success_deletion: successDeleted
                };
                callback(error, filesStatus);
            }

        });
    }
}

const createDirectoryAndRandomJSONFilesAndDelete = () => {
    const directoryName = 'RandomJSONFiles';
    createDirectory(directoryName, (error, directoryPath) => {
        if (error && error.code !== 'EEXIST') {
            console.error(`COULDN'T CREATE DIRECTORY`);
        } else {
            if (error && error.code === 'EEXIST') {
                console.log(`SKIPPING DIRECTORY CREATION, AS IT ALREADY EXISTS`);
            }
            console.log(`CREATING RANDOM JSON FILES`);
            createRandomJSONfiles(directoryPath, (error, files) => {
                if (error) {
                    console.error(`ERROR: ALL FILES WERE NOT CREATED`);
                } else {
                    console.log(`SUCCESS: ALL FILES WERE CREATED`);
                }
                console.log(`SUCCESSFULLY CREATED FILES: ${files['created_files'].length}`);
                console.log(files['created_files']);
                console.log(`FAILED IN CREATING FILES: ${files['failed_files'].length}`);
                console.log(files['failed_files']);

                const filesToBeDeleted = files['created_files'];

                deleteFiles(filesToBeDeleted, (error, filesStatus) => {
                    if (error) {
                        console.error(`ERROR: ALL FILES WERE NOT DELETED`);
                    } else {
                        console.log(`SUCCESS: ALL FILES WERE DELETED`);
                    }
                    console.log(`SUCCESSFULLY DELETED FILES: ${filesStatus['success_deletion'].length}`);
                    console.log(filesStatus['success_deletion']);
                    console.log(`FAILED IN CREATING FILES: ${filesStatus['failed_deletion'].length}`);
                    console.log(filesStatus['failed_deletion']);
                });
            });
        }
    });
};

module.exports = createDirectoryAndRandomJSONFilesAndDelete;
