const fs = require('fs');
const path = require('path');
const FILETOREAD = path.join(__dirname, './lipsum.txt');

const createReadWriteDeleteFiles = () => {
    fs.readFile(FILETOREAD, 'utf-8', (error, fileData) => {
        if (error) {
            console.error(`ERROR: Reading file: ${FILETOREAD}`, error);
        } else {
            console.log(`SUCCESS: Reading file: ${FILETOREAD}`);
            const upperCasedFileName = path.join(__dirname,'uppercased_lipsum.txt');
            fs.writeFile(upperCasedFileName, fileData.toUpperCase(), (error) => {
                if (error) {
                    console.error(`ERROR: Writing uppercased data to File ${upperCasedFileName}`, error);
                } else {
                    console.log(`SUCCESS: Writing uppercased data to ${upperCasedFileName}`);
                    const fileNameHolder = path.join(__dirname,'./filenames.txt');
                    fs.writeFile(fileNameHolder, upperCasedFileName.concat('\n'), { flag: 'a+' }, (error) => {
                        if (error) {
                            console.error(`ERROR: Writing ${upperCasedFileName} in file ${fileNameHolder}`, error);
                        } else {
                            console.log(`SUCCESS: Writing filename ${upperCasedFileName} in file ${fileNameHolder}`);
                            fs.readFile(upperCasedFileName, 'utf-8', (error, data) => {
                                if (error) {
                                    console.error(`ERROR: reading file${upperCasedFileName}`, error);
                                } else {
                                    const lowerCasedAndSentencedFileName = path.join(__dirname,'lowercased_and_sentenced.txt');
                                    const newData = data.toLowerCase().split('.')
                                        .map((sentence) => {
                                            return sentence.trim();
                                        })
                                        .filter((sentence) => {
                                            return sentence !== '';
                                        })
                                        .join('\n');
                                    fs.writeFile(lowerCasedAndSentencedFileName, newData, (error) => {
                                        if (error) {
                                            console.error(`ERROR: Writing lowercased and sentenced data to ${lowerCasedAndSentencedFileName}`, error);
                                        } else {
                                            console.log(`SUCCESS: writing lowercased and sentenced data to ${lowerCasedAndSentencedFileName}`);
                                            fs.writeFile(fileNameHolder, lowerCasedAndSentencedFileName.concat('\n'), { flag: 'a+' }, (error) => {
                                                if (error) {
                                                    console.error(`ERROR: writing filename ${lowerCasedAndSentencedFileName} in ${fileNameHolder}`, error);
                                                } else {
                                                    console.log(`SUCCESS: writing filename ${lowerCasedAndSentencedFileName} in ${fileNameHolder}`);
                                                    fs.readFile(upperCasedFileName, 'utf-8', (error, upperCasedData) => {
                                                        if (error) {
                                                            console.error(`ERROR: Reading ${upperCasedFileName} for sorting`, error);
                                                        } else {
                                                            console.log(`SUCCESS: Reading ${upperCasedFileName} for sorting`);
                                                            const sortedUpperCasedData = upperCasedData.replaceAll(' ', '').replaceAll('\n', '').split('').sort().join('');
                                                            const sortedUpperCaseFileName = path.join(__dirname,'uppercase_sorted.txt');
                                                            fs.writeFile(sortedUpperCaseFileName, sortedUpperCasedData, (error) => {
                                                                if (error) {
                                                                    console.error(`ERROR: writing sorted uppercased data in file ${sortedUpperCaseFileName}`, error);
                                                                } else {
                                                                    console.log(`SUCCESS: writing sorted uppercased data in file ${sortedUpperCaseFileName}`);
                                                                    fs.writeFile(fileNameHolder, sortedUpperCaseFileName.concat('\n'), { flag: 'a+' }, (error) => {
                                                                        if (error) {
                                                                            console.error(`ERROR: Writing ${sortedUpperCaseFileName} in ${fileNameHolder}`, error);
                                                                        } else {
                                                                            console.log(`SUCCESS: Writing ${sortedUpperCaseFileName} in ${fileNameHolder}`);
                                                                            fs.readFile(lowerCasedAndSentencedFileName, 'utf-8', (error, data) => {
                                                                                if (error) {
                                                                                    console.error(`ERROR: Reading file ${lowerCasedAndSentencedFileName}`, error);
                                                                                } else {
                                                                                    console.log(`SUCCESS: Reading file ${lowerCasedAndSentencedFileName}`);
                                                                                    const sortedLowerCasedAndSentencedData = data.replaceAll(' ', '').replaceAll('\n', '').split('').sort().join('');
                                                                                    const sortedlowerCasedAndSentencedFileName = path.join(__dirname,'lowedcased_sentenced_sorted.txt');
                                                                                    fs.writeFile(sortedlowerCasedAndSentencedFileName, sortedLowerCasedAndSentencedData, (error) => {
                                                                                        if (error) {
                                                                                            console.error(`ERROR: writing sorted content from ${sortedlowerCasedAndSentencedFileName} to ${sortedlowerCasedAndSentencedFileName}`, error);
                                                                                        } else {
                                                                                            console.log(`SUCCESS: writing sorted content from ${sortedlowerCasedAndSentencedFileName} to ${sortedlowerCasedAndSentencedFileName}`);
                                                                                            fs.writeFile(fileNameHolder, sortedlowerCasedAndSentencedFileName.concat('\n'), { flag: 'a+' }, (error) => {
                                                                                                if (error) {
                                                                                                    console.error(`ERROR: writing ${sortedlowerCasedAndSentencedFileName} in ${fileNameHolder}`, error);
                                                                                                } else {
                                                                                                    console.log(`SUCCESS: writing ${sortedlowerCasedAndSentencedFileName} in ${fileNameHolder}`);
                                                                                                    fs.readFile(fileNameHolder, 'utf-8', (error, fileNames) => {
                                                                                                        if (error) {
                                                                                                            console.error(`ERROR: Reading File ${fileNameHolder} for deleting files in it`, error);
                                                                                                        } else {
                                                                                                            console.log(`SUCCCESS: Reading File ${fileNameHolder} for deleting files in it`);
                                                                                                            const filesToBeDeleted = fileNames.split('\n')
                                                                                                                .filter((fileName) => {
                                                                                                                    return fileName !== '';
                                                                                                                });

                                                                                                            for (let index = 0; index < filesToBeDeleted.length; index++) {

                                                                                                                fs.unlink(filesToBeDeleted[index], (error) => {
                                                                                                                    if (error) {
                                                                                                                        console.error(`ERROR: Deleting File ${filesToBeDeleted[index]}`, error);
                                                                                                                    } else {
                                                                                                                        console.log(`SUCCESS: Deleting File ${filesToBeDeleted[index]}`);
                                                                                                                    }

                                                                                                                });
                                                                                                                
                                                                                                            }
                                                                                                            console.log(`ALL WORK DONE!`);

                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });

                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });

                                                                }
                                                            });
                                                        }
                                                    });

                                                }
                                            });

                                        }
                                    });
                                }

                            });
                        }
                    });

                }
            });
        }

    });

}

module.exports = createReadWriteDeleteFiles;
